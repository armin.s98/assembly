from .constant import REGITER_8_BIT, REGISTER_X64, OPCODE


def ri_update_upcode(instruction, reg, data, opcode):
    mod, reg_code, register_code = '', '', ''
    if instruction in INSTRUCTIONS:
        opcode, mod, reg_code, register_code = type_1_operand_opcode(instruction, reg, data)
    return opcode, mod, reg_code, register_code


def type_1_operand_opcode(instruction, reg, data):
    index = INSTRUCTIONS.index(instruction)
    nbits = get_number_of_bits(data)
    mod = '11'
    register_code = get_register_code(reg)
    reg_code = OPCODE[instruction]['reg_code']
    if reg == 'al':
        if index % 2 == 0:
            code = "4"
        else:
            code = "c"
        opcode = str(index) + code
        mod = ''
        register_code = ''
        reg_code = ''
    elif reg == 'rax' and nbits != 8:
        if index % 2 == 0:
            code = "5"
        else:
            code = "d"
        opcode = str(index) + code
        mod = ''
        register_code = ''
        reg_code = ''
    else:
        if reg in REGITER_8_BIT:
            opcode = "80"
        elif nbits == 8:
            opcode = "83"
        else:
            opcode = "81"
    opcode_binary_format = '{0:08b}'.format(int(opcode, 16))
    return opcode_binary_format, mod, reg_code, register_code


def get_register_code(reg):
    code = ''
    if reg in REGISTER_X64.keys():
        code = REGISTER_X64[reg]
    return code


def get_number_of_bits(num):  # Given number in hex mode
    num = num[2:]
    num = int(num, 16)
    if num < 128:
        return 8
    else:
        return 16


INSTRUCTIONS = ['add', 'or', 'adc', 'sbb', 'and', 'sub', 'xor', 'cmp']
