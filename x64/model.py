from .constant import REGISTER_X64, OPCODE, REGITER_8_BIT, NEW_REGISTER_64, SCALE, REGISTER_BP
from .ri_exception import ri_update_upcode
prefix, rex, opcode, mod, sib, displacement, Data = None, None, None, None, None, None, None


def init(instruction, par1=None, par2=None):
    type_1 = get_parameter_type(par1)
    type_2 = get_parameter_type(par2)
    print(type_1 + type_2)
    code = FUNC[type_1 + type_2](instruction, par1, par2)
    code_hex_list = [hex(int(code[i * 8: (i + 1) * 8], 2)) for i in range(0, len(code) // 8)]
    print(code_hex_list)


def register_register(instruction, par1, par2):
    opcode = update_opcode_by_w(OPCODE[instruction]['rr'], par1)
    mod = '11'  # In this type we don't have displacement
    register_1_code = REGISTER_X64[par1]
    register_2_code = REGISTER_X64[par2]
    mod_rm_code = mod + register_2_code + register_1_code
    rex_code = ''
    if is_64_bit_register(par1):
        rex_code = get_rex_code(reg1=par1, reg3=par2)
    return rex_code + opcode + mod_rm_code


def get_parameter_type(par):
    if par is None:
        return ''
    if par in REGISTER_X64.keys():
        return 'r'
    elif '[' == par[0]:
        return 'm'
    elif '0x' in par:
        return 'i'
    else:
        return ''


def get_hex_code(num):
    return hex(int(num, 2))


def is_8_bit_register(reg):  # Given register to function
    if reg in REGITER_8_BIT:
        return True
    return False


def is_64_bit_register(reg):  # Given register to function
    if reg[0] == 'r':
        return True
    return False


def _get_rex_register_code(reg):
    if reg in NEW_REGISTER_64:
        return '1'
    else:
        return '0'


def get_rex_code(reg1=None, reg2=None, reg3=None):
    rex_code = '0100'
    rex_w_code = '1'
    reg1_rex_code = _get_rex_register_code(reg1)
    reg2_rex_code = _get_rex_register_code(reg2)
    reg3_rex_code = _get_rex_register_code(reg3)
    return rex_code + rex_w_code + reg1_rex_code + reg2_rex_code + reg3_rex_code


def update_opcode_by_w(opcode, reg):
    if is_8_bit_register(reg):
        opcode = opcode[:-1] + '0'
    return opcode


def register_immediate(instruction, par1, par2):
    opcode = update_opcode_by_w(OPCODE[instruction]['ri'], par1)
    opcode, mod, reg_code, register_1_code = ri_update_upcode(instruction, par1, par2, opcode)
    disp = fix_data(par2)
    print(disp)
    rex_code = ''
    if is_64_bit_register(par1):
        rex_code = get_rex_code(reg1=par1)
    print('r', rex_code, 'o', opcode, 'm', mod, 'reg', reg_code, register_1_code, disp)
    return rex_code + opcode + mod + reg_code + register_1_code + disp


def fix_data(data):
    data = data.strip()
    data = data[2:]
    x = [data[i * 2: (i + 1) * 2] for i in range(len(data) // 2)]
    x.reverse()
    x = ''.join(x)
    digits = [int(c, 16) for c in x]
    hex_format = ''.join([format(d, '04b') for d in digits])
    return hex_format


def register_memory(instruction, par1, par2):
    opcode = update_opcode_by_w(OPCODE[instruction]['rm'], par1)
    register_1_code = REGISTER_X64[par1]
    base, index, scale, displacement = get_memory_params(par2)
    mod = get_mod(base, index, scale, displacement)
    base, index, scale, displacement, rm_code = apply_memory_exception(base, index, scale, displacement, mod)
    sib_code = get_sib_code(base, index, scale)
    displacement_code = ''
    if displacement is not None:
        displacement_code = fix_data(displacement)
    return opcode + mod + register_1_code + rm_code + sib_code + displacement_code


def get_register_code(reg):
    code = ''
    if reg in REGISTER_X64.keys():
        code = REGISTER_X64[reg]
    return code


def get_memory_params(par):
    base, index, scale, displacement = None, None, None, None
    par = par.strip('[').strip(']')
    params = par.split('+')
    params = [x.strip() for x in params]
    base_flag = False
    for param in params:
        if '0x' in param:
            displacement = param
        elif '*' in param:
            index_field = param.split('*')
            index_field = [x.strip() for x in index_field]
            index = index_field[0]
            if len(index_field) > 1:
                scale = index_field[1]
        elif not base_flag:
            base = param
            base_flag = True
        else:
            index = param
            scale = '1'
    if index is not None and scale is None:
        scale = 1
    return base, index, scale, displacement


def apply_memory_exception(base, index, scale, displacement, mod):
    rm_code = '100'
    if base is not None and index is None:
        return None, None, None, displacement, get_register_code(base)
    if base is None and index is not None:
        displacement = fix_displacement_bits_in_hex(displacement, 32)
    if base in REGISTER_BP and displacement is None:
        displacement = fix_displacement_bits_in_hex(displacement, 8)
    if base is None:
        base = 'ebp'
    if index is None:
        index = 'esp'
    if scale is None:
        scale = '1'
    return base, index, scale, displacement, rm_code


def fix_displacement_bits_in_hex(displacement, bits):  # Given hex displacement to this function
    if displacement is None:
        displacement = ''
    displacement = displacement.strip()
    displacement = displacement[2:]
    bits = bits // 4
    if len(displacement) % 2 != 0:
        displacement = '0' + displacement
    for _ in range(bits - len(displacement)):
        displacement = '0' + displacement
    displacement = '0x' + displacement
    return displacement


def get_sib_code(base, index, scale):
    base_code = get_register_code(base)
    index_code = get_register_code(index)
    scale_code = ''
    if scale is not None:
        scale_code = SCALE[scale]
    return scale_code + index_code + base_code


def get_mod(base, index, scale, displacement):
    if base in REGISTER_BP and displacement is None:
        return '01'
    if displacement is None or base is None:
        return '00'
    nbit = get_number_of_bits(displacement)
    if nbit == 8:
        return '01'
    else:
        return '10'


def get_number_of_bits(num):
    num = num.strip('0x')
    num = int(num, 16)
    if num < 128:
        return 8
    else:
        return 16


def memory_register(par1, par2):
    pass


def memory_memory(par1, par2):
    pass


def memory_immediate(par1, par2):
    pass


def non_operand(instruction, par1, par2):
    return OPCODE[instruction]


def single_operand_register(instruction, par1, par2=None):
    opcode = update_opcode_by_w(OPCODE[instruction]['r'], par1)
    mod = '11'
    reg_code = '000'
    register_code = get_register_code(par1)
    mod_code = mod + reg_code + register_code
    rex_code = ''
    if is_64_bit_register(par1):
        rex_code = get_rex_code(reg3=par1)
    return rex_code + opcode + mod_code


def single_operand_memory(instruction, par1, par2=None):
    opcode = OPCODE[instruction]['m']


FUNC = {
    'rr': register_register,
    'ri': register_immediate,
    'rm': register_memory,
    '': non_operand,
    'r': single_operand_register
}
